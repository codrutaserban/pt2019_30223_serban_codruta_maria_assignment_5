package PT2019.demo.Tema5;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public interface Main {
	
	public int numberDays(List<MonitoredData>  data);
	
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		List<String> days = new ArrayList<String>();
		List<MonitoredData> data= new ArrayList<MonitoredData>();
		
		try (Stream<String> stream = Files.lines(Paths.get("Activities.txt"))) {

			list = stream
					.collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}
		list.forEach(s->{
			 String start= s.substring(0,19);
			 String stop= s.substring(21,40);
			 String act=s.substring(42).trim();
			 
			 MonitoredData m= new MonitoredData (start,stop,act);
			 data.add(m);			
		});
		
		
		
/*Count how many days of monitored data appears in the log.*/		
		Main nrDays= (List<MonitoredData>  d)->{
			for(MonitoredData t:d) {
				String s= t.toString();
			String start= s.substring(0,19);
			 String stop= s.substring(20,40);
			 if(days.isEmpty()) {
				 days.add(start.substring(0,10));
			 }
			 if(start.substring(0,10).compareTo(days.get(days.size()-1))!=0) {
				 days.add(start.substring(0,10));
			 }
			 if(stop.substring(0,10).compareTo(days.get(days.size()-1))!=0) {
				 days.add(stop.substring(0,10));
			 }
			 }
			 return days.size();
		};
		
		int i=0;
		i=nrDays.numberDays(data);
		System.out.println("Number of days: "+i+"\n");

/*Count how many times has appeared each activity over the entire monitoring period.
Return a map of type <String, Int> representing the mapping of activities to their count.*/
		 Map<String, Long> counting = data.stream().collect(
	                Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
	     System.out.println(counting+"\n");
	     
/*Count how many times has appeared each activity for each day over the monitoring period*/
	     Map<String,Map<String ,Long>> counting1 = data.stream().collect(
	    		 Collectors.groupingBy(MonitoredData::getDate,Collectors.groupingBy(MonitoredData::getActivity ,Collectors.counting())));
	     System.out.println(counting1+"\n");
	     
	    
	     /*For each line from the file map for the activity label the duration recorded on that line
(end_time-start_time)*/
	     data.stream().forEach(s->System.out.println(s.time()+" "+s.getActivity()));
	     
	     
	     /*For each activity compute the entire duration over the monitoring period */
	     System.out.println("\nTotal duration for each activity:");
	     Map<String, Integer> duration =  data.stream().collect(
         Collectors.toMap(MonitoredData::getActivity ,MonitoredData::getDuration,(oldValue, newValue) -> oldValue+newValue));    
        for(String j: duration.keySet()) {
        	int sec=0;
        	 int zi=0;
        	 int ore=0;
        	 int min=0;
        	 int total=duration.get(j);
        	 sec=total%60;
        	 total=total/60;
        	 if(total>=1440) {
        		 zi=(int)(total/1440);
        		 total=total-zi*1440;
        	 }
        	 if(total>=60) {
        		 ore=(int)(total/60);
        		 total=total-ore*60;
        	 }
        	 min=total;
        	 System.out.print(j+" ");
        	 if(zi!=0)  System.out.print("days:"+zi+" ");
        	 if(ore!=0) System.out.print("hours:"+ore+" ");
        	 System.out.print("min:"+min+" ");
        	 System.out.println("sec:"+sec);
         }
        
        
        
/*Filter the activities that have 90% of the monitoring records with duration less than 5
minutes*/
        System.out.println("\nActivities that have 90% of the monitoring records with duration less than 5 minute:");		
	Map<String, Map<Boolean, Long>> duration2 = data.stream()
			.collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.partitioningBy(s -> s.getDuration()<300,Collectors.counting())));
	
	Long nr;
	Set<String> s1=counting.keySet();//contine 
	Set<String> s2=duration2.keySet();
	List<String>cs2=new ArrayList<String>(s2); 
	List<String>cs1=new ArrayList<String>(s1);
	for(int j=0;j<s1.size();j++) {
		Set<Boolean> b= duration2.get(cs2.get(j)).keySet();
		List<Boolean>b1=new ArrayList<Boolean>(b); 
		nr=duration2.get(cs2.get(j)).get(b1.get(1));
		if(nr>=(int)(counting.get(cs1.get(j))*0.9)) {
		    System.out.println(cs2.get(j));
		}
    }
	}
}
