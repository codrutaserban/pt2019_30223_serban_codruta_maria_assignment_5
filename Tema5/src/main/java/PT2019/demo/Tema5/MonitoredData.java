package PT2019.demo.Tema5;

import java.util.Date;

public class MonitoredData {
	private String start;
	private String end;
	private String activity;
	
	public String getStart() {
		return start;
	}
	public MonitoredData(String s, String e, String a) {
		start=s;
		end=e;
		activity=a;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String toString () {
		String s= start+" "+end+ " "+ activity;
		return s;
	}
	
	public String getDate() {
		return start.substring(0,10);
	}
	@SuppressWarnings("deprecation")
	public int getDuration() {
		String startT=start.substring(11);
		startT.trim();
		String stopT=end.substring(11);
		stopT.trim();
		start.trim();
		end.trim();
		Date d1= new Date();
		d1.setSeconds(Integer.parseInt(start.substring(17)));
		d1.setMinutes(Integer.parseInt(start.substring(14,16)));
		d1.setHours(Integer.parseInt(start.substring(11,13)));
		d1.setDate(Integer.parseInt(start.substring(8,10)));
		d1.setMonth(Integer.parseInt(start.substring(5,7)));
		d1.setYear(Integer.parseInt(start.substring(0,4)));
		
		Date d2= new Date();
		d2.setSeconds(Integer.parseInt(end.substring(17)));
		d2.setMinutes(Integer.parseInt(end.substring(14,16)));
		d2.setHours(Integer.parseInt(end.substring(11,13)));
		d2.setDate(Integer.parseInt(end.substring(8,10)));
		d2.setMonth(Integer.parseInt(end.substring(5,7)));
		d2.setYear(Integer.parseInt(end.substring(0,4)));
		
		
		int t=0;
		if(d2.getYear()>=d1.getYear()) {
			t=d2.getYear()-d1.getYear();
		}
		if (d2.getMonth()>=d1.getMonth()) { 
				t=t*12+d2.getMonth()-d1.getMonth();
		}
		if(d2.getDate()>=d1.getDate()) {
			t=t*30+d2.getDate()-d1.getDate();
		}
		t=t*24+d2.getHours()-d1.getHours();
		t=t*60+d2.getMinutes()-d1.getMinutes();
		t=t*60+d2.getSeconds()-d1.getSeconds();
		return t;
	}
	
	public String time () {
		int i= getDuration();
		String s=new String();
		int zi=0;
   	 int ore=0;
   	 int min=0;
   	 int sec=0;
   	 sec=i%60;
   	 int total=i/60;
   	 if(total>=1440) {
   		 zi=(int)(total/1440);
   		 total=total-zi*1440;
   	 }
   	 if(total>=60) {
   		 ore=(int)(total/60);
   		 total=total-ore*60;
   	 }
   	 min=total;
   	 if(zi!=0)  s=s+"days:"+zi+" ";
   	 if(ore!=0) s=s+"hours:"+ore+" ";
   	 s=s+"min:"+min+" ";
   	 s=s+"sec:"+sec;
		return s;
	}
	
	
	
	
}
