# Expresii lambda si procesarea stream-urilor #

Aceasta aplicatie trebuie sa extraga datele despre activitatiile unei persoane pe parcursul
mai multor zile si din acestea trebuie sa extragem informatiile conform cerintelor.
Datele din fisierul text sunt imprtite in 3 coloane: start_time, end_time si activity_type.
Start_time si end_time sunt reprezentate sub foma yyyy_mm_dd hh_mm_ss, asfel indicand data
si ora exacta cand o anumitaactivitate a inceput sau cand s-a sfarsit.
Rezultatele dorite in urma indeplinirii fiecarei cerinte sunt afisate in consola, unele sub
forma unei structuri de date map (String,int)